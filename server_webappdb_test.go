package auxdb

import (
	"testing"
	"time"

	auxpb "git.autistici.org/ai3/tools/aux-db/proto"
)

var (
	findAppsByNameQuerySpec = &QuerySpec{
		SQL: `
SELECT
  resource_id,
  app_key,
  json_extract(value_json, '$.appname') as appname,
  json_extract(value_json, '$.version') as version,
  json_extract(value_json, '$.safeversion') as safeversion,
  json_extract(value_json, '$.state') as state,
  json_extract(value_json, '$.vulninfo') as vulninfo
FROM
  latest
WHERE
  type = 'webapp' AND
  appname = :app
`,
		Results: []QueryParam{
			{Name: "resource_id", Type: TypeString},
			{Name: "app_key", Type: TypeString},
			{Name: "appname", Type: TypeString},
			{Name: "version", Type: TypeString},
			{Name: "safeversion", Type: TypeString},
			{Name: "state", Type: TypeString},
			{Name: "vulninfo", Type: TypeString},
		},
	}

	countAppsQuerySpec = &QuerySpec{
		SQL: `
SELECT
  COUNT(*) AS c,
  json_extract(value_json, '$.appname') as appname,
  json_extract(value_json, '$.version') as version
  FROM latest
WHERE
  type = 'webapp'
  AND appname = :app
GROUP BY appname, version
ORDER BY c DESC
`,
		Results: []QueryParam{
			{Name: "count", Type: TypeInt},
			{Name: "appname", Type: TypeString},
			{Name: "version", Type: TypeString},
		},
	}
)

func (c *testCtx) loadWebappTestData(t testing.TB) {
	c.jsonRequest(t, "/api/load", &auxpb.LoadRequest{
		Type:      "webapp",
		Timestamp: time.Now(),
		TTL:       60,
		Entries: []auxpb.LoadEntry{
			{
				ResourceID: "website1",
				AppKey:     "/path/1",
				ValueJSON: `
{"appname": "Wordpress", "version": "4.5.1", "safeversion": "5.6",
 "state": "vulnerable", "vulninfo": "CVE-2019-17672"}`,
			},
			{
				ResourceID: "website1",
				AppKey:     "/path/2",
				ValueJSON: `
{"appname": "Wordpress-plugin", "version": "0.1", "state": "ok"}`,
			},
			{
				ResourceID: "website2",
				AppKey:     "/path/3",
				ValueJSON: `
{"appname": "Wordpress", "version": "5.6", "state": "ok"}`,
			},
		},
	}, nil)
}

func TestWebapp_Load(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadWebappTestData(t)
	c.assertTableCount(t, "latest", 3)
}

func TestWebapp_Get(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadWebappTestData(t)

	var resp auxpb.GetResponse
	c.jsonRequest(t, "/api/get", &auxpb.GetRequest{
		Keys: []auxpb.Key{
			{
				Shard:      "1",
				Type:       "webapp",
				ResourceID: "website1",
			},
		},
	}, &resp)
	if len(resp.Results) != 2 {
		t.Fatalf("expected 2 results, got %d: %v", len(resp.Results), resp.Results)
	}
	for _, entry := range resp.Results {
		if entry.Key.AppKey != "/path/1" && entry.Key.AppKey != "/path/2" {
			t.Errorf("invalid app_key in result: %s", entry.Key.AppKey)
		}
	}
}

func TestWebapp_Query_Find(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadWebappTestData(t)

	// Now run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "webapp_find_by_name",
		Params: []auxpb.QueryParam{
			{Name: "app", Value: "Wordpress"},
		},
	}, &resp)
	if len(resp.Results) != 2 {
		t.Fatalf("expected 2 results, got %d: %v", len(resp.Results), resp.Results)
	}
	// Spot check a column.
	if resp.Results[0][3].(string) != "4.5.1" {
		t.Fatalf("bad result: %v", resp.Results[0])
	}
	for _, r := range resp.Results {
		appKey := r[1].(string)
		if appKey != "/path/1" && appKey != "/path/3" {
			t.Errorf("invalid app_key in result: %s", appKey)
		}
	}
}

func TestWebapp_Query_Count(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadWebappTestData(t)

	// Now run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "webapp_count_by_name",
		Params: []auxpb.QueryParam{
			{Name: "app", Value: "Wordpress"},
		},
	}, &resp)
	if len(resp.Results) != 2 {
		t.Fatalf("expected 1 result, got %d: %v", len(resp.Results), resp.Results)
	}
	// Spot check a column.
	if resp.Results[0][0].(float64) != 1 {
		t.Fatalf("bad result: %v", resp.Results[0])
	}
}
