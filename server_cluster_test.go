package auxdb

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"

	auxpb "git.autistici.org/ai3/tools/aux-db/proto"
)

// A type matching the clientutil.Backend interface that will resolve
// to our test servers.

type testClusterCtx struct {
	nodes  map[string]*testCtx
	client *http.Client
}

func newTestClusterCtx(t testing.TB, dbdir string, n int) *testClusterCtx {
	var cc testClusterCtx
	cc.client = new(http.Client)
	cc.nodes = make(map[string]*testCtx)
	var addrs []string
	for i := 0; i < n; i++ {
		shardID := strconv.Itoa(i + 1)
		dbPath := filepath.Join(dbdir, fmt.Sprintf("shard-%s.db", shardID))
		cc.nodes[shardID] = newTestCtx(t, shardID, dbPath, &cc)
		addrs = append(addrs, cc.nodes[shardID].http.URL)
	}
	log.Printf("test cluster, dbdir=%s, addrs=%s", dbdir, strings.Join(addrs, ", "))
	return &cc
}

func (c *testClusterCtx) shards() []string {
	var shards []string
	for s := range c.nodes {
		shards = append(shards, s)
	}
	return shards
}

func (c *testClusterCtx) randomNode() *testCtx {
	for _, nodeCtx := range c.nodes {
		return nodeCtx
	}
	return nil
}

func (c *testClusterCtx) Call(ctx context.Context, shardID, uri string, req, resp interface{}) error {
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	uri = c.nodes[shardID].http.URL + uri
	httpReq, err := http.NewRequest("POST", uri, bytes.NewReader(data))
	if err != nil {
		return err
	}
	httpReq.Header.Set("Content-Type", "application/json")
	httpReq.Header.Set("Content-Length", strconv.FormatInt(int64(len(data)), 10))

	httpResp, err := c.client.Do(httpReq.WithContext(ctx))
	if err != nil {
		return err
	}
	defer httpResp.Body.Close()
	if httpResp.StatusCode != 200 {
		return fmt.Errorf("http status %d", httpResp.StatusCode)
	}
	if err := json.NewDecoder(httpResp.Body).Decode(resp); err != nil {
		return err
	}
	return nil
}

func (c *testClusterCtx) Get(ctx context.Context, shardID, uri string) (*http.Response, error) {
	return nil, errors.New("not implemented")
}

func (c *testClusterCtx) Close() {
	for _, nodeCtx := range c.nodes {
		nodeCtx.Close()
	}
}

func (c *testClusterCtx) loadTestData(t testing.TB) {
	for _, nodeCtx := range c.nodes {
		nodeCtx.loadTestData(t)
	}
}

func TestDistributedQuery(t *testing.T) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	c := newTestClusterCtx(t, dir, 5)
	defer c.Close()

	c.loadTestData(t)

	// Now run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.randomNode().jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "top_usage",
		Params: []auxpb.QueryParam{
			{Name: "limit", Value: 3},
		},
		Shards: c.shards(),
	}, &resp)
	if len(resp.Results) != 3 {
		t.Errorf("expected 3 results, got %d", len(resp.Results))
	}
	if resp.Partial {
		t.Errorf("partial query response")
	}
	for _, res := range resp.Results {
		rsrcID := res[0].(string)
		usage := res[1].(float64)
		if rsrcID == "" || usage == 0 {
			t.Errorf("got result with wrong column types: %v", res)
		}
	}
}
