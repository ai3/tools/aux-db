package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/serverutil"
	auxdb "git.autistici.org/ai3/tools/aux-db"
	yaml "gopkg.in/yaml.v3"
)

type config struct {
	DBURI      string                      `yaml:"db_uri"`
	ShardID    string                      `yaml:"shard_id"`
	Peers      *clientutil.BackendConfig   `yaml:"peers"`
	HTTPConfig *serverutil.ServerConfig    `yaml:"http_server"`
	Queries    map[string]*auxdb.QuerySpec `yaml:"queries"`
}

var (
	addr       = flag.String("addr", ":3420", "tcp `address` to listen to")
	configPath = flag.String("config", "/etc/aux-db/server.yml", "configuration `file`")
)

// Build the server.Config object out of command-line flags.
func loadConfig(cfg *config) error {
	data, err := ioutil.ReadFile(*configPath)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(data, cfg); err != nil {
		return err
	}
	return nil
}

func run() error {
	var cfg config
	if err := loadConfig(&cfg); err != nil {
		return fmt.Errorf("could not load configuration: %v", err)
	}

	peers, err := clientutil.NewBackend(cfg.Peers)
	if err != nil {
		return fmt.Errorf("failed to instantiate peers service: %v", err)
	}

	db, err := auxdb.OpenDB(cfg.DBURI)
	if err != nil {
		return fmt.Errorf("failed to open database: %v", err)
	}
	defer db.Close()

	server := auxdb.NewServer(db, cfg.ShardID, cfg.Queries, peers, log.New(os.Stderr, "", 0))

	return serverutil.Serve(server, cfg.HTTPConfig, *addr)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if err := run(); err != nil {
		log.Fatalf("error: %v", err)
	}
}
