FROM golang:1.21.0 AS build

ADD . /src
WORKDIR /src
RUN go build -trimpath -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo,sqlite_omit_load_extension,json1 -o /aux-db-server ./cmd/aux-db-server

# We need to use Debian as the base because we need to dlopen libsqlite.
FROM scratch
COPY --from=build /aux-db-server /usr/bin/aux-db-server

ENTRYPOINT ["/usr/bin/aux-db-server"]
