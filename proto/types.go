package auxpb

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
	"time"
)

type Key struct {
	Shard      string
	Type       string
	ResourceID string
	AppKey     string
}

func ParseKey(s string) (k Key, err error) {
	parts := strings.SplitN(s, "/", 4)
	if len(parts) < 3 {
		err = errors.New("invalid key format")
		return
	}
	k.Shard, err = url.PathUnescape(parts[0])
	if err != nil {
		return
	}
	k.Type, err = url.PathUnescape(parts[1])
	if err != nil {
		return
	}
	k.ResourceID, err = url.PathUnescape(parts[2])
	if err != nil {
		return
	}
	if len(parts) == 4 {
		k.AppKey, err = url.PathUnescape(parts[3])
	}
	return
}

func (k Key) String() string {
	s := fmt.Sprintf(
		"%s/%s/%s",
		url.PathEscape(k.Shard),
		url.PathEscape(k.Type),
		url.PathEscape(k.ResourceID),
	)
	if k.AppKey != "" {
		s += "/"
		s += url.PathEscape(k.AppKey)
	}
	return s
}

// RPC request/response types.

// Entry uses 'raw' JSON for its ValueJSON field: you can replace the
// GetResponse with your own result type matching this JSON data
// fingerprint to have automated decoding via clientutil.Call().
type Entry struct {
	Key       Key         `json:"key"`
	ValueJSON EncodedJSON `json:"value_json"`
	Timestamp time.Time   `json:"timestamp"`
}

type SetRequest struct {
	Type       string    `json:"type"`
	ResourceID string    `json:"resource_id"`
	AppKey     string    `json:"app_key"`
	ValueJSON  string    `json:"value_json"`
	Timestamp  time.Time `json:"timestamp"`
	TTL        int       `json:"ttl"`
}

type LoadEntry struct {
	ResourceID string `json:"resource_id"`
	AppKey     string `json:"app_key"`
	ValueJSON  string `json:"value_json"`
}

type LoadRequest struct {
	Type      string      `json:"type"`
	Timestamp time.Time   `json:"timestamp"`
	TTL       int         `json:"ttl"`
	Entries   []LoadEntry `json:"entries"`
}

type GetRequest struct {
	Keys []Key `json:"keys"`
}

type GetResponse struct {
	Results []*Entry `json:"results"`
}

type QueryParam struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
}

type QueryRequest struct {
	QueryName string       `json:"query_name"`
	Params    []QueryParam `json:"params"`
	Shards    []string     `json:"shards"`
}

func (r *QueryRequest) ParamsMap() map[string]interface{} {
	m := make(map[string]interface{})
	for _, p := range r.Params {
		m[p.Name] = p.Value
	}
	return m
}

type QueryResponse struct {
	Results [][]interface{} `json:"results"`
	Partial bool            `json:"partial"`
}

// An 'encoded JSON' type that encodes to native JSON.
type EncodedJSON string

func (s EncodedJSON) MarshalJSON() ([]byte, error) {
	return []byte(s), nil
}

func (s *EncodedJSON) UnmarshalJSON(data []byte) error {
	*s = EncodedJSON(string(data))
	return nil
}
