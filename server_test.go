package auxdb

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	auxpb "git.autistici.org/ai3/tools/aux-db/proto"
)

var (
	// Note: Go sqlite3 module only supports the ':name' named
	// parameter syntax, not '@name' or others:
	// https://github.com/mattn/go-sqlite3/issues/697
	topUsageQuerySpec = &QuerySpec{
		SQL:       "SELECT resource_id, json_extract(value_json, '$.usage') AS usage FROM latest ORDER BY usage DESC LIMIT :limit",
		ReduceSQL: "SELECT resource_id, usage FROM :table ORDER BY usage DESC LIMIT :limit",
		Results: []QueryParam{
			{
				Name: "resource_id",
				Type: TypeString,
			},
			{
				Name: "usage",
				Type: TypeInt,
			},
		},
	}
	historicalUsageQuerySpec = &QuerySpec{
		SQL: "SELECT timestamp, json_extract(value_json, '$.usage') AS usage FROM historical WHERE type = :type AND resource_id = :resource_id ORDER BY timestamp DESC LIMIT 100",
		Results: []QueryParam{
			{
				Name: "timestamp",
				Type: TypeTimestamp,
			},
			{
				Name: "usage",
				Type: TypeInt,
			},
		},
	}

	testQueries = map[string]*QuerySpec{
		"top_usage":            topUsageQuerySpec,
		"historical_usage":     historicalUsageQuerySpec,
		"webapp_find_by_name":  findAppsByNameQuerySpec,
		"webapp_count_by_name": countAppsQuerySpec,
	}

	testResourcesData = &auxpb.LoadRequest{
		Type: "quota",
		//Timestamp: time.Now(),
		TTL: 60,
		Entries: []auxpb.LoadEntry{
			{
				ResourceID: "resource1",
				ValueJSON:  "{\"usage\":10}",
			},
			{
				ResourceID: "resource2",
				ValueJSON:  "{\"usage\":20}",
			},
			{
				ResourceID: "resource3",
				ValueJSON:  "{\"usage\":30}",
			},
			{
				ResourceID: "resource4",
				ValueJSON:  "{\"usage\":40}",
			},
			{
				ResourceID: "resource5",
				ValueJSON:  "{\"usage\":50}",
			},
		},
	}
)

type testCtx struct {
	db      *sql.DB
	srv     *Server
	http    *httptest.Server
	client  *http.Client
	dbPath  string
	shardID string
}

func newTestCtx(t testing.TB, shardID, dbPath string, peers clientutil.Backend) *testCtx {
	path := dbPath
	if path == "" {
		path = ":memory:"
	}
	db, err := OpenDB(path)
	if err != nil {
		t.Fatal(err)
	}
	srv := NewServer(db, shardID, testQueries, peers, log.New(os.Stderr, fmt.Sprintf("%s: ", shardID), 0))
	httpServer := httptest.NewServer(srv)
	return &testCtx{
		db:      db,
		srv:     srv,
		http:    httpServer,
		dbPath:  dbPath,
		shardID: shardID,
		client: &http.Client{
			Transport: http.DefaultTransport,
		},
	}
}

func createTestCtx(t testing.TB) *testCtx {
	return newTestCtx(t, "1", "", nil)
}

func createDiskBackedTestCtx(t testing.TB) *testCtx {
	return newTestCtx(t, "1", fmt.Sprintf("test-%X.db", rand.Int31()), nil)
}

func (c *testCtx) Close() {
	c.http.Close()
	c.db.Close()
	if c.dbPath != "" {
		os.Remove(c.dbPath)
	}
}

func (c *testCtx) jsonRequest(t testing.TB, url string, req, out interface{}) {
	encoded, _ := json.Marshal(req)
	resp, err := c.client.Post(c.http.URL+url, "application/json", bytes.NewReader(encoded))
	if err != nil {
		t.Fatalf("%s: %v", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Fatalf("%s: bad HTTP response status %s", url, resp.Status)
	}
	if out != nil {
		if err := json.NewDecoder(resp.Body).Decode(out); err != nil {
			t.Fatalf("%s: error decoding response: %v", url, err)
		}
	}
}

func (c *testCtx) loadTestData(t testing.TB) {
	// Load also some historical data.
	n := 10
	ts := time.Now().AddDate(0, 0, -n)
	for i := 0; i < n; i++ {
		ts = ts.AddDate(0, 0, 1)
		data := testResourcesData
		data.Timestamp = ts
		c.jsonRequest(t, "/api/load", data, nil)
	}
}

func (c *testCtx) assertTableCount(t testing.TB, table string, expected int) {
	row := c.db.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM %s", table))
	var rowCount int
	if err := row.Scan(&rowCount); err != nil {
		t.Fatalf("count(*) scan error: %v", err)
	}
	if rowCount != expected {
		t.Fatalf("expected %d entries in table %s, found %d",
			expected, table, rowCount)
	}
}

func TestLoad(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	// Load data and verify that the elements have been inserted.
	c.loadTestData(t)
	c.assertTableCount(t, "latest", 5)
	// 5 elements * 10 time values.
	c.assertTableCount(t, "historical", 50)
}

func TestGet(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	// Load data and verify that the elements have been inserted.
	c.loadTestData(t)

	var resp auxpb.GetResponse
	c.jsonRequest(t, "/api/get", &auxpb.GetRequest{
		Keys: []auxpb.Key{
			{
				Type:       "quota",
				Shard:      "1",
				ResourceID: "resource1",
			},
			{
				Type:       "quota",
				Shard:      "1",
				ResourceID: "resource2",
			},
		},
	}, &resp)
	if len(resp.Results) != 2 {
		t.Fatalf("expected 2 results, got %d", len(resp.Results))
	}
	for _, r := range resp.Results {
		if r.Key.String() != "1/quota/resource1" && r.Key.String() != "1/quota/resource2" {
			t.Errorf("bad key: %s", r.Key)
		}
		if r.ValueJSON == "" {
			t.Errorf("empty value_json")
		}
	}
}

func TestGet_NoResults(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	// Load data and verify that the elements have been inserted.
	c.loadTestData(t)

	var resp auxpb.GetResponse
	c.jsonRequest(t, "/api/get", &auxpb.GetRequest{
		Keys: []auxpb.Key{
			{
				Type:       "quota",
				Shard:      "1",
				ResourceID: "unknown-resource",
			},
		},
	}, &resp)
	if len(resp.Results) != 0 {
		t.Fatalf("expected 0 results, got %d", len(resp.Results))
	}
}

func TestSet(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.jsonRequest(t, "/api/set", &auxpb.SetRequest{
		Type:       "quota",
		ResourceID: "resource1",
		Timestamp:  time.Now(),
		TTL:        60,
	}, nil)
	c.assertTableCount(t, "latest", 1)
	c.assertTableCount(t, "historical", 1)
}

func TestQuery(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadTestData(t)

	// Now run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "top_usage",
		Params: []auxpb.QueryParam{
			{Name: "limit", Value: 3},
		},
	}, &resp)
	if len(resp.Results) != 3 {
		t.Fatalf("expected 3 results, got %d", len(resp.Results))
	}
	for _, res := range resp.Results {
		rsrcID := res[0].(string)
		usage := res[1].(float64)
		if rsrcID == "" || usage == 0 {
			t.Errorf("got result with wrong column types: %v", res)
		}
	}
}

func TestQuery_NoResults(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadTestData(t)

	// Now run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "top_usage",
		Params: []auxpb.QueryParam{
			{Name: "limit", Value: 0},
		},
	}, &resp)
	if len(resp.Results) != 0 {
		t.Fatalf("expected 0 results, got %d", len(resp.Results))
	}
}

func TestQueryWithParams(t *testing.T) {
	c := createTestCtx(t)
	defer c.Close()

	c.loadTestData(t)

	// Run the test query and verify that the results can be
	// correctly deserialized into the expected column types.
	var resp auxpb.QueryResponse
	c.jsonRequest(t, "/api/query", &auxpb.QueryRequest{
		QueryName: "historical_usage",
		Params: []auxpb.QueryParam{
			{Name: "type", Value: "quota"},
			{Name: "resource_id", Value: "resource1"},
		},
	}, &resp)
	if len(resp.Results) != 10 {
		t.Fatalf("expected 10 results, got %d", len(resp.Results))
	}
	for _, res := range resp.Results {
		usage := res[1].(float64)
		if usage != 10 {
			t.Errorf("retrieved bad usage: expected=10, actual=%g", usage)
		}
	}
}

func BenchmarkSet(b *testing.B) {
	c := createDiskBackedTestCtx(b)
	defer c.Close()

	b.RunParallel(func(pb *testing.PB) {
		rr := rand.New(rand.NewSource(rand.Int63()))
		for pb.Next() {
			key := fmt.Sprintf("user%06X", rr.Intn(100))
			value := fmt.Sprintf("{\"usage\": %d}", rr.Intn(1000000))
			c.jsonRequest(b, "/api/set", &auxpb.SetRequest{
				Type:       "quota",
				ResourceID: key,
				ValueJSON:  value,
				Timestamp:  time.Now(),
				TTL:        600,
			}, nil)
		}
	})
}
