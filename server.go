package auxdb

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/go-common/sqlutil"
	auxpb "git.autistici.org/ai3/tools/aux-db/proto"
)

var horizon = 365 * 24 * time.Hour

type Server struct {
	db      *sql.DB
	shardID string
	queries map[string]*QuerySpec
	peers   clientutil.Backend
	stopCh  chan struct{}

	log *log.Logger
}

func NewServer(db *sql.DB, shardID string, queries map[string]*QuerySpec, peers clientutil.Backend, log *log.Logger) *Server {
	s := &Server{
		db:      db,
		shardID: shardID,
		queries: queries,
		peers:   peers,
		stopCh:  make(chan struct{}),
		log:     log,
	}
	go s.expireLoop()
	return s
}

func (s *Server) Close() {
	close(s.stopCh)
}

// Handy structure to insert records in parallel in the "latest" and
// "historical" tables.
type preparedInserts struct {
	latest     *sql.Stmt
	historical *sql.Stmt
}

// Prepare a pair of insert statements. We close these later when
// calling preparedInserts.Close()
// nolint: sqlclosecheck
func prepareInserts(tx *sql.Tx) (ins preparedInserts, err error) {
	ins.latest, err = tx.Prepare(
		"INSERT OR REPLACE INTO latest (type, resource_id, app_key, value_json, timestamp, expiry) VALUES (?, ?, ?, ?, ?, ?)")
	if err != nil {
		return
	}
	ins.historical, err = tx.Prepare(
		"INSERT INTO historical (type, resource_id, app_key, value_json, timestamp) VALUES (?, ?, ?, ?, ?)")
	return
}

func (i *preparedInserts) Exec(key auxpb.Key, valueJSON string, timestamp time.Time, ttl int) error {
	if _, err := i.latest.Exec(
		key.Type,
		key.ResourceID,
		key.AppKey,
		valueJSON,
		timestamp,
		timestamp.Add(time.Second*time.Duration(ttl)),
	); err != nil {
		return err
	}
	_, err := i.historical.Exec(
		key.Type,
		key.ResourceID,
		key.AppKey,
		valueJSON,
		timestamp,
	)
	return err
}

func (i *preparedInserts) Close() {
	i.latest.Close()
	i.historical.Close()
}

// Remove obsolete records from the "latest" table.
func (s *Server) expireLatest() error {
	return sqlutil.WithTx(context.Background(), s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM latest WHERE expiry < ?", time.Now())
		return err
	})
}

// Drop ancient records from the "historical" table.
func (s *Server) expireHistorical() error {
	return sqlutil.WithTx(context.Background(), s.db, func(tx *sql.Tx) error {
		cutoff := time.Now().Add(-horizon)
		_, err := tx.Exec("DELETE FROM historical WHERE timestamp < ?", cutoff)
		return err
	})
}

func (s *Server) expireLoop() {
	t := time.NewTicker(6 * time.Hour)
	for {
		select {
		case <-s.stopCh:
			return
		case <-t.C:
			if err := s.expireLatest(); err != nil {
				s.log.Printf("error expiring entries from the 'latest' table: %v", err)
			}
			if err := s.expireHistorical(); err != nil {
				s.log.Printf("error expiring entries from the 'historical' table: %v", err)
			}
		}
	}
}

func (s *Server) handleSet(w http.ResponseWriter, r *http.Request) {
	var req auxpb.SetRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	key := auxpb.Key{
		Shard:      s.shardID,
		Type:       req.Type,
		ResourceID: req.ResourceID,
		AppKey:     req.AppKey,
	}

	err := sqlutil.WithTx(r.Context(), s.db, func(tx *sql.Tx) error {
		ins, err := prepareInserts(tx)
		if err != nil {
			return err
		}
		defer ins.Close()

		return ins.Exec(
			key,
			req.ValueJSON,
			req.Timestamp,
			req.TTL,
		)
	})
	if err != nil {
		http.Error(w, "Database error", http.StatusInternalServerError)
		s.log.Printf("set: tx: %s", err)
		return
	}

	s.log.Printf("set(%s)", key)
	serverutil.EncodeJSONResponse(w, nil)
}

func (s *Server) handleLoad(w http.ResponseWriter, r *http.Request) {
	var req auxpb.LoadRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	err := sqlutil.WithTx(r.Context(), s.db, func(tx *sql.Tx) error {
		ins, err := prepareInserts(tx)
		if err != nil {
			return err
		}
		defer ins.Close()

		for _, entry := range req.Entries {
			if err := ins.Exec(
				auxpb.Key{
					Shard:      s.shardID,
					Type:       req.Type,
					ResourceID: entry.ResourceID,
					AppKey:     entry.AppKey,
				},
				entry.ValueJSON,
				req.Timestamp,
				req.TTL,
			); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		http.Error(w, "Database error", http.StatusInternalServerError)
		s.log.Printf("load: tx: %s", err)
		return
	}

	s.log.Printf("load(%d entries)", len(req.Entries))
	serverutil.EncodeJSONResponse(w, nil)
}

func (s *Server) scanEntries(tx *sql.Tx, key auxpb.Key) ([]*auxpb.Entry, error) {
	var rows *sql.Rows
	var err error

	// Linter is confused by the delayed error check.
	// nolint: rowserrcheck
	if key.AppKey == "" {
		rows, err = tx.Query(`
		SELECT
			type, resource_id, app_key, value_json, timestamp
		FROM latest
		WHERE
			type = ?
			AND resource_id = ?
`, key.Type, key.ResourceID)
	} else {
		rows, err = tx.Query(`
		SELECT
			type, resource_id, app_key, value_json, timestamp
		FROM latest
		WHERE
			type = ?
			AND resource_id = ?
			AND app_key = ?
`, key.Type, key.ResourceID, key.AppKey)
	}

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var results []*auxpb.Entry
	for rows.Next() {
		var e auxpb.Entry
		e.Key.Shard = s.shardID
		if err := rows.Scan(&e.Key.Type, &e.Key.ResourceID, &e.Key.AppKey, &e.ValueJSON, &e.Timestamp); err != nil {
			return nil, err
		}
		results = append(results, &e)
	}

	return results, rows.Err()
}

func (s *Server) handleGet(w http.ResponseWriter, r *http.Request) {
	var req auxpb.GetRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	var results []*auxpb.Entry
	err := sqlutil.WithReadonlyTx(r.Context(), s.db, func(tx *sql.Tx) error {
		for _, key := range req.Keys {
			entries, err := s.scanEntries(tx, key)
			if err != nil {
				return err
			}
			results = append(results, entries...)
		}
		return nil
	})
	if err != nil && err != sql.ErrNoRows {
		http.Error(w, "Database error", http.StatusInternalServerError)
		s.log.Printf("get: tx: %s", err)
		return
	}

	s.log.Printf("get(%v): %d results", req.Keys, len(results))
	serverutil.EncodeJSONResponse(w, &auxpb.GetResponse{
		Results: results,
	})
}

func (s *Server) handleQuery(w http.ResponseWriter, r *http.Request) {
	var req auxpb.QueryRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	query, ok := s.queries[req.QueryName]
	if !ok {
		s.log.Printf("query: unknown query '%s'", req.QueryName)
		http.Error(w, "No such query", http.StatusBadRequest)
		return
	}

	var partial bool
	var results [][]interface{}
	err := sqlutil.WithReadonlyTx(r.Context(), s.db, func(tx *sql.Tx) (err error) {
		// Should we run a MR? Only if a few conditions are met.
		if len(req.Shards) > 0 && query.ReduceSQL != "" {
			results, partial, err = mapAndReduce(
				r.Context(), tx, s.peers, &req, query, s.shardID, s.log)
		} else {
			results, err = runLocalQuery(tx, &req, query)
		}
		return
	})
	if err != nil && err != sql.ErrNoRows {
		http.Error(w, "Database error", http.StatusInternalServerError)
		s.log.Printf("query: tx: %s", err)
		return
	}

	s.log.Printf("query(%+v): %d results", req, len(results))
	serverutil.EncodeJSONResponse(w, &auxpb.QueryResponse{
		Results: results,
		Partial: partial,
	})
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	switch r.URL.Path {
	case "/api/get":
		s.handleGet(w, r)
	case "/api/set":
		s.handleSet(w, r)
	case "/api/load":
		s.handleLoad(w, r)
	case "/api/query":
		s.handleQuery(w, r)
	default:
		http.NotFound(w, r)
	}
}
