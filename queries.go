package auxdb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"regexp"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	auxpb "git.autistici.org/ai3/tools/aux-db/proto"
)

// NullString converts database NULLs to empty strings.
type NullString string

func (s *NullString) Scan(value interface{}) error {
	if value == nil {
		*s = ""
		return nil
	}
	strVal, ok := value.(string)
	if !ok {
		return errors.New("column is not a string")
	}
	*s = NullString(strVal)
	return nil
}

const (
	TypeInt       = "int"
	TypeString    = "string"
	TypeTimestamp = "timestamp"
)

type QueryParam struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type QuerySpec struct {
	SQL           string       `yaml:"sql"`
	Results       []QueryParam `yaml:"results"`
	ReduceSQL     string       `yaml:"reduce_sql"`
	ReduceResults []QueryParam `yaml:"reduce_results"`
}

// Run the 'primary' query.
func (q *QuerySpec) runPrimaryQuery(tx *sql.Tx, params map[string]interface{}) ([][]interface{}, error) {
	return q.runQuery(tx, q.SQL, params, q.Results, "")
}

// Run the 'reduce' query (which has an additional named argument
// 'table' referring to the temporary table name).
func (q *QuerySpec) runReduceQuery(tx *sql.Tx, params map[string]interface{}, table string) ([][]interface{}, error) {
	// Fallback to results if reduce_results is not specified.
	resultsSpec := q.ReduceResults
	if len(resultsSpec) == 0 {
		resultsSpec = q.Results
	}
	return q.runQuery(tx, q.ReduceSQL, params, resultsSpec, table)
}

var tableSQLRx = regexp.MustCompile(`[:@\$]table\b`)

// Low-level SQL query executor that parses results into fundamental
// Go data types (see scanRow() below) according to the query spec.
func (q *QuerySpec) runQuery(tx *sql.Tx, query string, params map[string]interface{}, resultsSpec []QueryParam, reduceTable string) ([][]interface{}, error) {
	// Build the named parameter list, and run the query.
	var args []interface{}
	for name, param := range params {
		args = append(args, sql.Named(name, param))
	}

	// We could have set :table as a named parameter, but the SQL
	// parser refuses to let us use a parameter in the FROM
	// target, so we do manual string substitution instead.
	if reduceTable != "" {
		// args = append(args, sql.Named("table", reduceTable))
		query = tableSQLRx.ReplaceAllString(query, reduceTable)
	}

	rows, err := tx.Query(query, args...)
	if err != nil {
		return nil, fmt.Errorf("query error: %v", err)
	}
	defer rows.Close()

	// Load the results into columnar format based on the result
	// metadata we want.
	var results [][]interface{}
	for rows.Next() {
		row, err := scanResultRow(rows, resultsSpec)
		if err != nil {
			return nil, fmt.Errorf("scan error: %v", err)
		}
		results = append(results, row)
	}
	return results, rows.Err()
}

// Parse a row of results into fundamental Go types, according to the
// query specification.
func scanResultRow(rows *sql.Rows, resultSpec []QueryParam) ([]interface{}, error) {
	var results []interface{}
	for _, r := range resultSpec {
		switch r.Type {
		case TypeInt:
			results = append(results, new(int))
		case TypeString:
			results = append(results, new(NullString))
		case TypeTimestamp:
			results = append(results, new(time.Time))
		}
	}

	err := rows.Scan(results...)
	return results, err
}

// Map our data types into SQL ones.
var sqlTypes = map[string]string{
	TypeInt:       "INTEGER",
	TypeString:    "TEXT",
	TypeTimestamp: "DATETIME",
}

// Create a temporary SQL table with a schema based on a parameter
// specification.
func createTempTable(tx *sql.Tx, resultsSpec []QueryParam) (string, error) {
	table := fmt.Sprintf("temp.aggregate_%x", rand.Int63())

	var spec []string
	for _, r := range resultsSpec {
		spec = append(spec, fmt.Sprintf("%s %s", r.Name, sqlTypes[r.Type]))
	}
	s := fmt.Sprintf("CREATE TEMP TABLE %s (%s)", table, strings.Join(spec, ", "))

	_, err := tx.Exec(s)
	return table, err
}

func deleteTempTable(tx *sql.Tx, table string) error {
	_, err := tx.Exec(fmt.Sprintf("DROP TABLE %s", table))
	return err
}

func buildTempInsertStmt(table string, resultsSpec []QueryParam) string {
	var cols, placeholders []string
	for _, r := range resultsSpec {
		cols = append(cols, r.Name)
		placeholders = append(placeholders, "?")
	}
	return fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES (%s)",
		table,
		strings.Join(cols, ", "),
		strings.Join(placeholders, ", "),
	)
}

func loadTempTable(tx *sql.Tx, table string, resultsSpec []QueryParam, results [][]interface{}) error {
	stmt, err := tx.Prepare(buildTempInsertStmt(table, resultsSpec))
	if err != nil {
		return err
	}
	defer stmt.Close()
	for _, row := range results {
		_, err := stmt.Exec(row...)
		if err != nil {
			return err
		}
	}
	return nil
}

func runRemoteQuery(ctx context.Context, be clientutil.Backend, shard string, req *auxpb.QueryRequest) (*auxpb.QueryResponse, error) {
	// Make a copy of the request and clear the 'shard' attribute,
	// which will ensure that no reduce stage is executed
	// remotely.
	creq := *req
	creq.Shards = nil

	var resp auxpb.QueryResponse
	if err := be.Call(ctx, shard, "/api/query", &creq, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}

// Aggregate results from other shards, load them into a temporary SQL
// table, and run a 'reduce' query on them. The bool return value
// represents whether we had any trouble talking to the remote shards.
func mapAndReduce(ctx context.Context, tx *sql.Tx, be clientutil.Backend, req *auxpb.QueryRequest, query *QuerySpec, localShardID string, log *log.Logger) ([][]interface{}, bool, error) {
	// Create the temporary table where the results will be
	// aggregated.
	table, err := createTempTable(tx, query.Results)
	if err != nil {
		return nil, false, err
	}
	defer func() {
		if err := deleteTempTable(tx, table); err != nil {
			log.Printf("mr: failed to delete temporary table: %v", err)
		}
	}()

	var partial bool
	var wg sync.WaitGroup
	for _, shard := range req.Shards {
		wg.Add(1)
		go func(shard string) {
			defer wg.Done()

			// Run the RPC query and load the results into the
			// temporary table.
			results, err := runMrQuery(ctx, tx, be, req, table, query, shard, localShardID)
			if err != nil {
				log.Printf("mr: %v", err)
				partial = true
				return
			}

			if err := loadTempTable(tx, table, query.Results, results); err != nil {
				log.Printf("mr: failed to load results from shard %s: %v", shard, err)
				partial = true
			}
		}(shard)
	}

	wg.Wait()

	results, err := query.runReduceQuery(tx, req.ParamsMap(), table)
	return results, partial, err
}

func runMrQuery(ctx context.Context, tx *sql.Tx, be clientutil.Backend, req *auxpb.QueryRequest, table string, query *QuerySpec, shard, localShardID string) ([][]interface{}, error) {
	// Run the RPC query. Avoid making a nested RPC into the local server
	// (just run the query directly).
	var results [][]interface{}
	switch {
	case shard == localShardID:
		lresults, err := runLocalQuery(tx, req, query)
		if err != nil {
			return nil, fmt.Errorf("local query failed: %w", err)
		}
		results = lresults
	default:
		resp, err := runRemoteQuery(ctx, be, shard, req)
		if err != nil {
			return nil, fmt.Errorf("remote query failed on shard %s: %w", shard, err)
		}
		results = resp.Results
	}
	return results, nil
}

// Run the query locally - meant to mirror mapAndReduce() above.
func runLocalQuery(tx *sql.Tx, req *auxpb.QueryRequest, query *QuerySpec) ([][]interface{}, error) {
	return query.runPrimaryQuery(tx, req.ParamsMap())
}
