package auxdb

import (
	"database/sql"

	"git.autistici.org/ai3/go-common/sqlutil"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE latest (
    type TEXT NOT NULL,
    resource_id TEXT NOT NULL,
    app_key TEXT,
    value_json TEXT,
    timestamp DATETIME NOT NULL,
    expiry DATETIME NOT NULL
);
`, `
CREATE UNIQUE INDEX idx_latest_composite_key ON latest(type, resource_id, app_key);
`, `
CREATE INDEX idx_latest_resource_key ON latest(type, resource_id);
`, `
CREATE INDEX idx_latest_type ON latest(type);
`, `
CREATE TABLE historical (
    type TEXT NOT NULL,
    resource_id TEXT NOT NULL,
    app_key TEXT,
    value_json TEXT,
    timestamp DATETIME NOT NULL
);
`, `
CREATE INDEX idx_historical_composite_key ON historical(type, resource_id);
`, `
CREATE INDEX idx_historical_full_key ON historical(type, resource_id, app_key);
`, `
CREATE INDEX idx_historical_type ON historical(type);
`),
}

// OpenDB opens a SQLite database and runs the database migrations.
func OpenDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(migrations))
}
